let http = require('http')

let course = [
	{
		"name": "HTML"
	},
	{
		"name": "JAVASCRIPT"
	}
]


let port = 4000

const server = http.createServer(function(request, response) {
	
	if(request.url == '/' && request.method == 'GET') {	
	response.writeHead(200, {'Content-Type': 'text/plain'})
	response.end('Welcome to Booking System!')
	} else if(request.url == '/profile' && request.method == 'GET') { 
	response.writeHead(200, {'Content-Type': 'text/plain'})
	response.end('Welcome to your profile!');
	} else if(request.url == '/course' && request.method == 'GET') { 
	response.writeHead(200, {'Content-Type': 'text/plain'})
	response.end('Here\'s our courses available!');
	} else if(request.url == '/course' && request.method == 'POST') { 

	let request_data = ''
	request.on('data', function(data){
			request_data += data
			console.log(request_data)
		})

	request.on('end', function() {
			
			request_data = JSON.parse(request_data)
	
			let new_course = {
				"name": request_data.name
			}
			console.log(new_course)
			course.push(new_course)
			console.log(course)
			
			response.writeHead(200, {'Content-Type': 'application/json'})
			response.write(JSON.stringify(new_course))
			response.end()

		})
	} else if(request.url == '/updateCourse' && request.method == 'PUT') { 
	response.writeHead(200, {'Content-Type': 'text/plain'})
	response.end('Update a course to our resourses!');
	} else if(request.url == '/deleteCourse' && request.method == 'DELETE') { 
	response.writeHead(200, {'Content-Type': 'text/plain'})
	response.end('Archive courses to our resourses!');
	}
}).listen(port);

console.log(`Server is now accessible at localhost: ${port}`);

