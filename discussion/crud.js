let http = require('http');

// Mock database:
let users = [
	{
		"name": "Jose Marie Chan",
		"email": "jmarie@gmail.com"
	},
	{
		"name": "Mariah Carey",
		"email": "mariahwhistle@gmail.com"
	}
];

let port = 4000;

let server = http.createServer(function(req, res) {
	if(req.url == '/users' && req.method == 'GET') {
		res.writeHead(200, {'Content-Type': 'application/json'})
		res.write(JSON.stringify(users))
		res.end()
	} else if (req.url == '/users' && req.method == 'POST') {
		// Declare a placeholder variable in order to be reassigned later on. This variable will be assigned the data inside the body from postman app.
		let request_body = ' '

		// When data is detected, run a function that assigns that data to the empty request_body variable.
		// req.on can have either of these 3 values/events: data, end, error
		req.on('data', function(data){
			request_body += data
			console.log(request_body)
		})
		// Before the request ends, we want to add the new data to our existing users array of objects.
		req.on('end', function() {
			console.log(typeof request_body)

			// Convert the request_body from JSON to JS Object then assign the converted value back to the request_body variable.
			request_body = JSON.parse(request_body)

			// Declare a new_user variable signifying the new data that came from the POSTMAN body.
			let new_user = {
				"name": request_body.name,
				"email": request_body.email
			}
			console.log(new_user)
			// Add the new_user variable into the users array of objects.
			users.push(new_user)
			console.log(users)

			// Write the header for the response. Make sure the content type is 'application/json' since we are writing/returning a JSON
			res.writeHead(200, {'Content-Type': 'application/json'})
			// res.write - writes the desired response from our DB to the POSTMAN response section
			res.write(JSON.stringify(new_user))
			res.end()

		})
	}
})




// server.listen(port);

// console.log(`Server is running at localhost: ${port}`);

// let http = require ('http')

// let users = [
// 	{
// 		"name" : "Jose Marie Chan",
// 		"email" : "jmarie@gmail.com"
// 	},
// 	{
// 		"name" : "Mariah Carey",
// 		"email" : "mariahwhistle.gmail.com"
//  	}
// ]

// let port = 4000
// let server = http.createServer(function(req, res){
// 	if(req.url == '/users' && req.method == 'GET'){
// 		res.writeHead(200, {'Content-Type': 'application/json'})
// 		res.writeHead(JSON.stringify(users))
// 		res.end();

// 	} else if (req.url == '/users' && req.method == 'POST'){
// 		let request_body = ''

// 		req.on('data', function(data){
// 			request_body += data
// 			console.log(request_body)
// 		})
// 		req.on('end',function(){
// 			console.log(request_body)
// 			request_body = JSON.parse(request_body)
// 			let new_user = {
// 				"name" : request_body.name,
// 				"email" :request_body.email
// 			}
// 			console.log(new_user)
// 			users.push(new_user)
// 			console.log(users)

// 			res.writeHead(200, {'Content-Type': 'application/json'});
// 		res.writeHead(JSON.stringify(new_user))
// 		res.end()
// 		})
// 	}
// })
// server.listen(port);

// console.log(`Server is now accessible at localhost: ${port}`);
